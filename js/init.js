$(document).ready(function() {

	$(document).on("click", function() {
		if($(".menu-bar > div:first-child > div.dropdown").hasClass("open")) {
				$(".my-acc").addClass("drop-style");
		} else {
			if ($(".my-acc").hasClass("drop-style")) {
				$(".my-acc").removeClass("drop-style");
			} 
		}

		if($(".cart > div.dropdown").hasClass("open")) {
				$(".cart-menu").addClass("drop-style");
 
		} else {
			if ($(".cart-menu").hasClass("drop-style")) {
				$(".cart-menu").removeClass("drop-style");
			} 
		}		
	});
	
	

	$(".navbar-toggle").on("click", function(){
		$(".navbar-brand").toggleClass("hide");
	});

	$(".my-acc").on("click", function() {
		$(this).toggleClass("drop-style");
		if ($(".cart-menu").hasClass("drop-style")) {
			$(".cart-menu").removeClass("drop-style");
		}
	});

	$(".cart-menu").on("click", function() {
		$(this).toggleClass("drop-style");
		if ($(".my-acc").hasClass("drop-style")) {
			$(".my-acc").removeClass("drop-style");
		}
	});

	$('#carousel123').carousel({ interval: 4000});

	$('.carousel-showsixmoveone .item').each(function(){
		var itemToClone = $(this);

		for (var i=1;i<5;i++) {
			itemToClone = itemToClone.next();

			if (!itemToClone.length) {
				itemToClone = $(this).siblings(':first');
			}

			itemToClone.children(':first-child').clone()
			.addClass("cloneditem-"+(i))
			.appendTo($(this));
		}
	});

});

